package http

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"

	req "github.com/imroc/req/v3"
)

const BASE_URL = "https://api.linkedin.com/rest/"

func post(Url string, params interface{}, header map[string]string) (APIResponse, string) {
	client := req.C()
	var apiResponse APIResponse
	resp, err := client.R().
		SetHeaders(header).
		SetSuccessResult(&apiResponse).
		SetBody(params).
		Post(Url)
	if err != nil {
		return apiResponse, err.Error()
	}
	if !resp.IsSuccessState() {
		return apiResponse, resp.Status + " : " + resp.String()
	}
	return apiResponse, ""
}

func get(Url string, fields map[string]string, header map[string]string) (APIResponse, string) {
	client := req.C()
	var apiResponse APIResponse
	resp, err := client.R().
		SetHeaders(header).
		SetSuccessResult(&apiResponse).
		SetQueryParams(fields).
		Get(Url)
	if err != nil {
		return apiResponse, err.Error()
	}
	if !resp.IsSuccessState() {
		return apiResponse, resp.Status + " : " + resp.String()
	}

	return apiResponse, ""
}

func (apiRequest *APIRequest) Execute(ctx context.Context) (APIResponse, string) {
	var apiResponse APIResponse
	var errRes string
	header := map[string]string{
		"X-Restli-Protocol-Version": "2.0.0",
		"Content-Type":              "application/json",
		"Authorization":             "Bearer " + apiRequest.AccessToken,
		"Linkedin-Version":          "202404",
	}
	if apiRequest.URL == "" {
		if apiRequest.Path == "" {
			return apiResponse, "Invalid path or URL!"
		}
		apiRequest.URL = BASE_URL + apiRequest.Path
	}
	if apiRequest.RequestMethod == "POST" {
		apiResponse, errRes = post(apiRequest.URL, apiRequest.Params, header)
	} else if apiRequest.RequestMethod == "GET" {

		// apiResponse, err = get(apiRequest.URL, apiRequest.Fields, header)
		err := DoRequest(ctx, apiRequest.RequestMethod, apiRequest.URL, func(r *http.Request) {
			r.Header.Set("X-Restli-Protocol-Version", "2.0.0")
			r.Header.Set("Content-Type", "application/json")
			r.Header.Set("Authorization", "Bearer "+apiRequest.AccessToken)
			r.Header.Set("Linkedin-Version", "202404")
			rawQuery := ""
			for k, v := range apiRequest.Fields {
				if rawQuery == "" {
					rawQuery += k + "=" + v
					continue
				}
				rawQuery += "&" + k + "=" + v
			}
			r.URL.RawQuery = rawQuery
		}, apiRequest.Params, &apiResponse, &apiResponse)
		if err != nil {
			errRes = err.Error()
		}
	} else {
		errRes = "Invalid Request Method!"
	}
	return apiResponse, errRes
}

func DoRequest(
	ctx context.Context,
	method,
	url string,
	modifyRequest func(*http.Request),
	body interface{},
	response interface{},
	errorResponse interface{},
) error {
	// Convert body to []byte
	var reqBody []byte
	if body != nil {
		var ok bool
		reqBody, ok = body.([]byte)
		if !ok {
			// Convert body to JSON
			jsonBody, err := json.Marshal(body)
			if err != nil {
				return err
			}
			reqBody = jsonBody
		}
	}

	// Create a new request object
	req, err := http.NewRequestWithContext(ctx, method, url, bytes.NewReader(reqBody))
	if err != nil {
		return err
	}

	req.Header.Set("Content-Type", "application/json")

	// Set headers
	if modifyRequest != nil {
		modifyRequest(req)
	}

	client := &http.Client{}
	// Send the request and get the response
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer func() {
		err := resp.Body.Close()
		if err != nil {
			log.Printf("Failed to close the response body. Error Details: %s", err.Error())
		}
	}()

	// Read the response body
	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	// If the response status code is not in the 200-299 range, unmarshal the error onto the provided struct
	if resp.StatusCode < 200 || resp.StatusCode >= 300 {
		err = json.Unmarshal(respBody, &errorResponse)
		if err != nil {
			return err
		}
		return fmt.Errorf("request failed with status %d: %s", resp.StatusCode, errorResponse)
	}

	// Unmarshal the response onto the provided struct
	if response != nil {
		err = json.Unmarshal(respBody, &response)
		if err != nil {
			return err
		}
	}
	// Return nil if there were no errors
	return nil
}
