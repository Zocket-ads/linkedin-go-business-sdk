package http

type APIRequest struct {
	URL           string            `json:"url"`
	RequestMethod string            `json:"requestMethod"`
	Params        interface{}       `json:"params"`
	Fields        map[string]string `json:"fields"`
	AccessToken   string            `json:"accessToken"`
	Path          string            `json:"path"`
}

// Element represents an individual ad element
type Element struct {
	AdURL              string  `json:"adUrl"`
	Details            Details `json:"details"`
	IsRestricted       bool    `json:"isRestricted"`
	RestrictionDetails string  `json:"restrictionDetails"`
	Metadata           any     `json:"metadata"`
}

// Details contains detailed information about the ad
type Details struct {
	Advertiser   Advertiser    `json:"advertiser"`
	AdStatistics AdStatistics  `json:"adStatistics"`
	AdTargeting  []AdTargeting `json:"adTargeting"`
	Type         string        `json:"type"`
}

// Advertiser contains information about the advertiser
type Advertiser struct {
	AdvertiserName string `json:"advertiserName"`
	AdvertiserURL  string `json:"advertiserUrl"`
}

// AdStatistics contains statistics about the ad impressions
type AdStatistics struct {
	LatestImpressionAt   int64                 `json:"latestImpressionAt"`
	FirstImpressionAt    int64                 `json:"firstImpressionAt"`
	TotalImpressions     ImpressionRange       `json:"totalImpressions"`
	ImpressionsByCountry []ImpressionByCountry `json:"impressionsDistributionByCountry"`
}

// ImpressionRange represents the range of total impressions
type ImpressionRange struct {
	From int `json:"from"`
	To   int `json:"to"`
}

// ImpressionByCountry represents the distribution of impressions by country
type ImpressionByCountry struct {
	Country              string  `json:"country"`
	ImpressionPercentage float64 `json:"impressionPercentage"`
}

// AdTargeting represents the ad targeting criteria
type AdTargeting struct {
	IncludedSegments []string `json:"includedSegments"`
	FacetName        string   `json:"facetName"`
	ExcludedSegments []string `json:"excludedSegments"`
	IsIncluded       bool     `json:"isIncluded"`
	IsExcluded       bool     `json:"isExcluded"`
}

type Paging struct {
	Start float64 `json:"start"`
	Count float64 `json:"count"`
	Links []any   `json:"links"`
	Total float64 `json:"total"`
}

type APIResponse struct {
	Message         string      `json:"message"`
	Paging          Paging      `json:"paging"`
	Metadata        interface{} `json:"metadata"`
	Elements        []Element   `json:"elements"`
	ErrorDetailType interface{} `json:"errorDetailType"`
	ErrorDetails    interface{} `json:"errorDetails"`
	Status          int64       `json:"status"`
}
