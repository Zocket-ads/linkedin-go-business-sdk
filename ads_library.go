package linkedingobusinesssdk

import (
	"context"

	"gitlab.com/Zocket-ads/linkedin-go-business-sdk/http"
)

type AdClient struct {
	accessToken string
}

func NewAdsClient(accessToken string) *AdClient {
	return &AdClient{
		accessToken: accessToken,
	}
}

func (l *AdClient) GetAdsLibraryData(ctx context.Context, fields map[string]string) (http.APIResponse, string) {
	var apiRequest http.APIRequest
	apiRequest.AccessToken = l.accessToken
	apiRequest.Path = "adLibrary"
	apiRequest.RequestMethod = "GET"
	fields["q"] = "criteria"
	apiRequest.Fields = fields
	return apiRequest.Execute(ctx)
}
