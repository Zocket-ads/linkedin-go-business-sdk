package object

import (
	"context"

	"gitlab.com/Zocket-ads/linkedin-go-business-sdk/http"
)

type AdAccount struct {
	ID string `json:"id"`
}

func (adAccount *AdAccount) GetAuthorizedAdAccounts(ctx context.Context, accessToken string, fields map[string]string) (http.APIResponse, string) {
	var apiRequest http.APIRequest
	apiRequest.Path = "adAccounts?q=search"
	apiRequest.RequestMethod = "GET"
	apiRequest.Fields = fields
	apiRequest.AccessToken = accessToken
	return apiRequest.Execute(ctx)
}

func (adAccount *AdAccount) CreateCampaign(ctx context.Context, accessToken string, fields map[string]string) (http.APIResponse, string) {
	var apiRequest http.APIRequest
	apiRequest.Path = "adAccounts/" + adAccount.ID + "/adCampaigns"
	apiRequest.RequestMethod = "POST"
	apiRequest.Fields = fields
	apiRequest.AccessToken = accessToken
	return apiRequest.Execute(ctx)
}
